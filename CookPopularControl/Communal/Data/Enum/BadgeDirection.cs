﻿using CookPopularControl.Controls;



/*
 * Copyright (c) 2021 All Rights Reserved.
 * Description：BadgeDirection
 * Author： Chance_写代码的厨子
 * Create Time：2021-10-21 19:20:47
 */
namespace CookPopularControl.Communal.Data
{
    /// <summary>
    /// <see cref="Badge"/>位置
    /// </summary>
    public enum BadgeDirection
    {
        LeftTop,
        LeftBottom,
        RightTop,
        RightBottom
    }
}
