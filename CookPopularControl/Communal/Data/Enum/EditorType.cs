﻿/*
 * Copyright (c) 2021 All Rights Reserved.
 * Description：EditorType
 * Author： Chance_写代码的厨子
 * Create Time：2021-10-24 15:06:57
 */
namespace CookPopularControl.Communal.Data
{
    /// <summary>
    /// 可编辑标签元素的类型
    /// </summary>
    public enum EditorType : byte
    {
        TextBox,
        NumericUpDown,
        Other,
    }
}
